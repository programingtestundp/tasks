import 'package:flutter/material.dart';

import '../blocs/bloc_exports.dart';
import 'recycle_bin.dart';
import 'tabs_screen.dart';

class MyDrawer extends StatelessWidget {
  const MyDrawer({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Drawer(
          child: Column(
        children: [
          Container(
            width: double.infinity,
            padding: const EdgeInsets.symmetric(vertical: 14, horizontal: 20),
            color: Colors.grey,
            alignment: Alignment.center,
            child: Text(
              'Task Drawer',
              style: Theme.of(context).textTheme.headlineSmall,
            ),
          ),
          BlocBuilder<TasksBloc, TasksState>(builder: (context, stateX) {
            var state = stateX as TasksLoaded;
            return Builder(builder: (context) {
              return GestureDetector(
                onTap: () =>
                    Navigator.of(context).pushReplacementNamed(TabsScreen.id),
                child: ListTile(
                  leading: const Icon(Icons.folder_special),
                  title: const Text('My Tasks'),
                  trailing: Text(
                      '${state.pendingTasks.length} Pending | ${state.competedTasks.length} Completed'),
                ),
              );
            });
          }),
          const Divider(),
          BlocBuilder<TasksBloc, TasksState>(builder: (context, stateX) {
            var state = stateX as TasksLoaded;
            return Builder(builder: (context) {
              return GestureDetector(
                onTap: () =>
                    Navigator.of(context).pushReplacementNamed(RecycleBin.id),
                child: ListTile(
                  leading: const Icon(Icons.folder_special),
                  title: const Text('Bin'),
                  trailing: Text('${state.removedTasks.length}'),
                ),
              );
            });
          }),
          BlocBuilder<SwitchBloc, SwitchState>(
            builder: (context, stateX) {
              final state = stateX as SwitchLoaded;
              return Builder(builder: (context) {
                return Switch(
                  value: state.switchKey,
                  onChanged: (value) {
                    context.read<SwitchBloc>().add(SwitchKeyEvent());
                  },
                );
              });
            },
          )
        ],
      )),
    );
  }
}
