import 'package:flutter/material.dart';

import '../Widgets/task_list.dart';
import '../blocs/bloc_exports.dart';
import '../models/task.dart';

class FavoriteTasksScreen extends StatelessWidget {
  const FavoriteTasksScreen({super.key});
  static const id = 'tasks_screen';
  @override
  Widget build(BuildContext context) {
    return Builder(builder: (context) {
      return BlocBuilder<TasksBloc, TasksState>(
        builder: (context, state) {
          // if (state is TasksInitial) {
          //   return Center(child: CircularProgressIndicator());
          // }
          if (state is TasksLoaded) {
            List<Task> tasksList = state.favoriteTasks;

            return Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Center(
                    child: Chip(label: Text('${tasksList.length} Tasks')),
                  ),
                  TaskList(tasksList: tasksList)
                ]);
          } else {
            return const Text('Something went wrong');
          }
        },
      );
    });
  }
}
