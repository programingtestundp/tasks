import 'package:flutter/material.dart';

import '../blocs/bloc_exports.dart';
import '../models/task.dart';

Widget addTaskScreen(BuildContext context,
    TextEditingController titleController, TextEditingController descController,
    {Task? oldTask, bool isEdit = false}) {
  return Padding(
    padding: const EdgeInsets.all(20),
    child: Column(children: [
      const Text(
        'Add Task',
        style: TextStyle(fontSize: 24),
      ),
      const SizedBox(
        height: 10,
      ),
      TextField(
        autofocus: true,
        controller: titleController,
        decoration: const InputDecoration(
            label: Text('title'), border: OutlineInputBorder()),
      ),
      const SizedBox(
        height: 10,
      ),
      TextField(
        // autofocus: true,
        controller: descController,
        minLines: 3,
        maxLines: 5,
        decoration: const InputDecoration(
            label: Text('description'), border: OutlineInputBorder()),
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Text('cancel')),
          ElevatedButton(
              onPressed: () {
                var task = Task(
                    title: titleController.text,
                    description: descController.text,
                    date: DateTime.now().toString());
                !isEdit
                    ? {context.read<TasksBloc>().add(AddTask(task: task))}
                    : {
                        context
                            .read<TasksBloc>()
                            .add(EditTask(newTask: task, oldTask: oldTask!))
                      };
                Navigator.pop(context);
              },
              child: const Text('Add')),
        ],
      ),
    ]),
  );
}
