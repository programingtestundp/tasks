import 'package:flutter/material.dart';

import '../Widgets/add_task_widget.dart';
import '../blocs/bloc_exports.dart';
import 'completed_tasks_screen.dart';
import 'favorite_tasks_screen.dart';
import 'my_drawer.dart';
import 'pending_task_screen.dart';

// ignore: must_be_immutable
class TabsScreen extends StatelessWidget {
  TabsScreen({super.key});
  static const id = 'tabs_screen';

  TextEditingController titleController = TextEditingController();

  TextEditingController descController = TextEditingController();

  final List<Map<String, dynamic>> _pageDetails = [
    {'pageName': const PendingTasksScreen(), 'title': 'Pending Tasks'},
    {'pageName': const CompletedTasksScreen(), 'title': 'Completed Tasks'},
    {'pageName': const FavoriteTasksScreen(), 'title': 'Favorite Tasks'}
  ];

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TasksBloc, TasksState>(builder: (context, stateX) {
      final selectedPageIndex = (stateX as TasksLoaded).selectedPageIndex;
      return Builder(builder: (context) {
        return Scaffold(
          appBar: AppBar(
              title: Text(_pageDetails[selectedPageIndex]['title']),
              actions: [
                IconButton(
                    onPressed: () =>
                        onAddEditTask(context, titleController, descController),
                    icon: const Icon(Icons.add))
              ]),
          drawer: const MyDrawer(),
          body: _pageDetails[selectedPageIndex]['pageName'],
          floatingActionButton: selectedPageIndex == 0
              ? FloatingActionButton(
                  onPressed: () =>
                      onAddEditTask(context, titleController, descController),
                  tooltip: 'Add Task',
                  child: const Icon(Icons.add),
                )
              : null,
          bottomNavigationBar: BottomNavigationBar(
              currentIndex: selectedPageIndex,
              onTap: (value) {
                context.read<TasksBloc>().add(SwitchBody(index: value));
              },
              items: const [
                BottomNavigationBarItem(
                    icon: Icon(Icons.incomplete_circle_sharp),
                    label: 'Pending Tasks'),
                BottomNavigationBarItem(
                    icon: Icon(Icons.done), label: 'Competed Tasks'),
                BottomNavigationBarItem(
                    icon: Icon(Icons.favorite), label: 'Favorite Tasks'),
              ]),
        );
      });
    });
  }
}
