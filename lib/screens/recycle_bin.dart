import 'package:flutter/material.dart';

import '../Widgets/task_list.dart';
import '../blocs/bloc_exports.dart';
import 'my_drawer.dart';

class RecycleBin extends StatelessWidget {
  const RecycleBin({super.key});
  static const id = 'recycle_bin_screen';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: const MyDrawer(),
        appBar: AppBar(title: const Text('Recycle Bin'), actions: [
          IconButton(icon: const Icon(Icons.add), onPressed: () {})
        ]),
        body: BlocBuilder<TasksBloc, TasksState>(builder: (context, stateX) {
          var state = stateX as TasksLoaded;
          return Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Center(
                  child:
                      Chip(label: Text('${state.removedTasks.length} Tasks')),
                ),
                TaskList(tasksList: state.removedTasks),
              ]);
        }));
  }
}
