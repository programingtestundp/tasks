import 'package:flutter/material.dart';

import '../models/task.dart';
import '../screens/add_task_screen.dart';

onAddEditTask(BuildContext contextB, TextEditingController titleController,
    descController,
    {bool isEdit = false, Task? oldTask}) {
  showModalBottomSheet(
    context: contextB,
    isScrollControlled: true,
    builder: (context) => SingleChildScrollView(
      child: Container(
        padding:
            EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
        child: addTaskScreen(contextB, titleController, descController,
            isEdit: isEdit, oldTask: oldTask),
      ),
    ),
  );
}
