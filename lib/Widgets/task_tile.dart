import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../blocs/bloc_exports.dart';
import '../models/task.dart';
import 'add_task_widget.dart';
import 'popup_menu.dart';

// ignore: must_be_immutable
class TaskTile extends StatelessWidget {
  TaskTile({
    super.key,
    required this.task,
  });
  final Task task;
  void _removeOrDeleteTask(BuildContext ctx, Task task) {
    task.isDeleted!
        ? ctx.read<TasksBloc>().add(DeleteTask(task: task))
        : ctx.read<TasksBloc>().add(RemoveTask(task: task));
  }

  Future<void> _editTask(BuildContext context) async {
    titleController.text = task.title;
    descController.text = task.description;
    Future.delayed(
      const Duration(seconds: 0),
      () => onAddEditTask(context, titleController, descController,
          isEdit: true, oldTask: task),
    );
  }

  TextEditingController titleController = TextEditingController();

  TextEditingController descController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Row(
              children: [
                Icon(task.isFavorite! ? Icons.star : Icons.star_outline),
                const SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(task.title,
                          style: TextStyle(
                              decoration: task.isDone!
                                  ? TextDecoration.lineThrough
                                  : null)),
                      Text(DateFormat()
                          .add_yMMMd()
                          .add_Hms()
                          .format(DateTime.parse(task.date)))
                    ],
                  ),
                ),
              ],
            ),
          ),
          Row(
            children: [
              Checkbox(
                value: task.isDone,
                onChanged: !task.isDeleted!
                    ? (value) {
                        context.read<TasksBloc>().add(UpdateTask(task: task));
                      }
                    : null,
              ),
              PopupMenu(
                task: task,
                recycleCallback: () =>
                    context.read<TasksBloc>().add(RecycleTask(task: task)),
                bookmarkCallback: () =>
                    context.read<TasksBloc>().add(FavoriteTask(task: task)),
                editCallback: () => _editTask(context),
                cancelOrDeleteCallback: () =>
                    _removeOrDeleteTask(context, task),
              )
            ],
          ),
        ],
      ),
    );
  }
}
