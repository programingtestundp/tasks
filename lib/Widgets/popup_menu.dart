import 'package:flutter/material.dart';

import '../blocs/bloc_exports.dart';
import '../models/task.dart';

class PopupMenu extends StatelessWidget {
  const PopupMenu(
      {super.key,
      required this.cancelOrDeleteCallback,
      required this.editCallback,
      required this.recycleCallback,
      required this.task,
      required this.bookmarkCallback});
  final VoidCallback cancelOrDeleteCallback;
  final VoidCallback bookmarkCallback;
  final VoidCallback editCallback;
  final VoidCallback recycleCallback;
  final Task task;

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<TasksBloc, TasksState>(
      // listenWhen: (previous, current) => current is TasksLoaded,
      listener: (context, state) {
        // if (state is TasksLoaded) {}
      },
      buildWhen: (previous, current) => current is TasksLoaded,
      builder: (context, state) {
        if (state is TasksLoaded) {
          return PopupMenuButton(
              itemBuilder: !task.isDeleted!
                  ? _pendingList(context, state.selectedPageIndex)
                  : _deletedList(context));
        } else {
          return const SizedBox.shrink();
        }
      },
    );
  }

  _deletedList(BuildContext context) => (context) => [
        PopupMenuItem(
          onTap: recycleCallback,
          child: TextButton.icon(
              onPressed: null,
              icon: const Icon(Icons.restore_from_trash),
              label: const Text('Restore')),
        ),
        PopupMenuItem(
          onTap: cancelOrDeleteCallback,
          child: TextButton.icon(
              onPressed: null,
              icon: const Icon(Icons.delete_forever),
              label: const Text('Delete Forever')),
        ),
      ];

  _pendingList(BuildContext context, int index) => (context) => [
        if (index != 2)
          PopupMenuItem(
            onTap: editCallback,
            child: TextButton.icon(
              onPressed: null,
              icon: const Icon(Icons.edit),
              label: const Text('Edit'),
            ),
          ),
        PopupMenuItem(
          onTap: bookmarkCallback,
          child: TextButton.icon(
              onPressed: null,
              icon: task.isFavorite!
                  ? const Icon(Icons.bookmark_remove)
                  : const Icon(Icons.bookmark_add_outlined),
              label: task.isFavorite!
                  ? const Text('Remove from Bookmark')
                  : const Text('Add to Bookmark')),
        ),
        if (index != 2)
          PopupMenuItem(
            onTap: cancelOrDeleteCallback,
            child: TextButton.icon(
                onPressed: null,
                icon: const Icon(Icons.delete),
                label: const Text('Delete')),
          ),
      ];
}
