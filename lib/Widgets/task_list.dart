import 'package:flutter/material.dart';

import '../models/task.dart';
import 'task_tile.dart';

class TaskList extends StatelessWidget {
  const TaskList({
    super.key,
    required this.tasksList,
  });

  final List<Task> tasksList;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: SingleChildScrollView(
        child: ExpansionPanelList.radio(
          children: tasksList
              .map((task) => ExpansionPanelRadio(
                  value: task.id!,
                  headerBuilder: (context, isOpen) => TaskTile(
                        task: task,
                      ),
                  body: Align(
                    alignment: const Alignment(-.9, 0),
                    child: SelectableText.rich(
                        textAlign: TextAlign.left,
                        TextSpan(children: [
                          const TextSpan(
                              text: 'Title\n',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(
                            text: task.title,
                          ),
                          const TextSpan(
                              text: '\n\nDescriptoin\n',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(
                            text: task.description,
                          ),
                        ])),
                  )))
              .toList(),
        ),
      ),
    );
  }
}
