// ignore_for_file: public_member_api_docs, sort_constructors_first
// import 'dart:convert';

part of 'switch_bloc.dart';

abstract class SwitchState extends Equatable {
  const SwitchState();

  @override
  List<Object> get props => [];
}

class SwitchInitial extends SwitchState {}

// ignore: must_be_immutable
class SwitchLoaded extends SwitchState {
  bool switchKey;
  SwitchLoaded({
    required this.switchKey,
  });
  @override
  List<Object> get props => [switchKey];

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'switchKey': switchKey,
    };
  }

  factory SwitchLoaded.fromMap(Map<String, dynamic> map) {
    return SwitchLoaded(
      switchKey: map['switchKey'] as bool,
    );
  }

  // String toJson() => json.encode(toMap());

  // factory SwitchLoaded.fromJson(String source) => SwitchLoaded.fromMap(json.decode(source) as Map<String, dynamic>);
}
