import '../bloc_exports.dart';

part 'switch_event.dart';
part 'switch_state.dart';

class SwitchBloc extends HydratedBloc<SwitchEvent, SwitchState> {
  SwitchBloc() : super(SwitchLoaded(switchKey: false)) {
    on<SwitchKeyEvent>((event, emit) {
      if (state is SwitchLoaded) {
        final state = this.state as SwitchLoaded;
        emit(SwitchLoaded(switchKey: !state.switchKey));
      }
    });
  }

  @override
  SwitchState? fromJson(Map<String, dynamic> json) {
    return SwitchLoaded.fromMap(json);
  }

  @override
  Map<String, dynamic>? toJson(SwitchState state) {
    if (state is SwitchLoaded) {
      return state.toMap();
    } else {
      throw UnimplementedError();
    }
  }
}
