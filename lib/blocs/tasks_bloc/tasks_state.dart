part of 'tasks_bloc.dart';

class TasksState extends Equatable {
  const TasksState();
  @override
  List<Object> get props => [];
}

class TasksInitial extends TasksState {}

class TasksLoaded extends TasksState {
  final List<Task> pendingTasks;
  final List<Task> competedTasks;
  final List<Task> favoriteTasks;
  final List<Task> removedTasks;
  final int selectedPageIndex;
  const TasksLoaded(
      {this.pendingTasks = const <Task>[],
      this.competedTasks = const <Task>[],
      this.favoriteTasks = const <Task>[],
      this.removedTasks = const <Task>[],
      this.selectedPageIndex = 0});

  @override
  List<Object> get props => [
        pendingTasks,
        favoriteTasks,
        competedTasks,
        selectedPageIndex,
        removedTasks
      ];

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'pendingTasks': pendingTasks.map((x) => x.toMap()).toList(),
      'competedTasks': competedTasks.map((x) => x.toMap()).toList(),
      'favoriteTasks': favoriteTasks.map((x) => x.toMap()).toList(),
      'removedTasks': removedTasks.map((x) => x.toMap()).toList(),
      'selectedPageIndex': selectedPageIndex
    };
  }

  factory TasksLoaded.fromMap(Map<String, dynamic> map) {
    return TasksLoaded(
        pendingTasks: List<Task>.from(
          (map['pendingTasks'] as List).map<Task>(
            (x) => Task.fromMap(x as Map<String, dynamic>),
          ),
        ),
        competedTasks: List<Task>.from(
          (map['competedTasks'] as List).map<Task>(
            (x) => Task.fromMap(x as Map<String, dynamic>),
          ),
        ),
        favoriteTasks: List<Task>.from(
          (map['favoriteTasks'] as List).map<Task>(
            (x) => Task.fromMap(x as Map<String, dynamic>),
          ),
        ),
        removedTasks: List<Task>.from(
          (map['removedTasks'] as List).map<Task>(
            (x) => Task.fromMap(x as Map<String, dynamic>),
          ),
        ),
        selectedPageIndex: map['selectedPageIndex'] as int);
  }

  // String toJson() => json.encode(toMap());

  // factory TasksLoaded.fromJson(String source) =>
  //     TasksLoaded.fromMap(json.decode(source) as Map<String, dynamic>);
}
