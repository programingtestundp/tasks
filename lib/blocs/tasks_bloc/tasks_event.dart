part of 'tasks_bloc.dart';

abstract class TasksEvent extends Equatable {
  const TasksEvent();

  @override
  List<Object> get props => [];
}

class LoadTasks extends TasksEvent {}

class SwitchBody extends TasksEvent {
  final int index;
  const SwitchBody({
    required this.index,
  });
  @override
  List<Object> get props => [index];
}

class AddTask extends TasksEvent {
  final Task task;
  const AddTask({
    required this.task,
  });
  @override
  List<Object> get props => [task];
}

class EditTask extends TasksEvent {
  final Task newTask;
  final Task oldTask;
  const EditTask({
    required this.newTask,
    required this.oldTask,
  });
  @override
  List<Object> get props => [newTask, oldTask];
}

class UpdateTask extends TasksEvent {
  final Task task;
  const UpdateTask({
    required this.task,
  });
  @override
  List<Object> get props => [task];
}

class FavoriteTask extends TasksEvent {
  final Task task;
  const FavoriteTask({
    required this.task,
  });
  @override
  List<Object> get props => [task];
}

class DeleteTask extends TasksEvent {
  final Task task;
  const DeleteTask({
    required this.task,
  });
  @override
  List<Object> get props => [task];
}

class RemoveTask extends TasksEvent {
  final Task task;
  const RemoveTask({
    required this.task,
  });
  @override
  List<Object> get props => [task];
}

class RecycleTask extends TasksEvent {
  final Task task;
  const RecycleTask({
    required this.task,
  });
  @override
  List<Object> get props => [task];
}
