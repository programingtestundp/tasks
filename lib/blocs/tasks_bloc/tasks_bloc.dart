import '../../models/task.dart';
import '../bloc_exports.dart';
part 'tasks_state.dart';
part 'tasks_event.dart';

class TasksBloc extends HydratedBloc<TasksEvent, TasksState> {
  TasksBloc() : super(const TasksLoaded()) {
    on<SwitchBody>(_onSwitchBody);
    on<AddTask>(_onAddTask);
    on<UpdateTask>(_onUpdateTask);
    on<FavoriteTask>(_onFavorite);
    on<EditTask>(_onEditTask);
    on<RecycleTask>(_onRecycleTask);
    on<DeleteTask>(_onDeleteTask);
    on<RemoveTask>(_onRemoveTask);
  }

  void _onSwitchBody(SwitchBody event, Emitter<TasksState> emit) {
    if (state is TasksLoaded) {
      final state = this.state as TasksLoaded;
      emit(TasksLoaded(
        pendingTasks: state.pendingTasks,
        favoriteTasks: state.favoriteTasks,
        competedTasks: state.competedTasks,
        removedTasks: state.removedTasks,
        selectedPageIndex: event.index,
      ));
    }
  }

  void _onAddTask(AddTask event, Emitter<TasksState> emit) {
    if (state is TasksLoaded) {
      final state = this.state as TasksLoaded;
      emit(TasksLoaded(
        pendingTasks: List.from(state.pendingTasks)..add(event.task),
        favoriteTasks: state.favoriteTasks,
        competedTasks: state.competedTasks,
        removedTasks: state.removedTasks,
        selectedPageIndex: state.selectedPageIndex,
      ));
    }
  }

  void _onUpdateTask(UpdateTask event, Emitter<TasksState> emit) {
    if (state is TasksLoaded) {
      final state = this.state as TasksLoaded;
      final task = event.task;
      // final int index = state.pendingTasks.indexOf(task);
      List<Task> pendingTasks = state.pendingTasks;
      List<Task> competedTasks = state.competedTasks;
      List<Task> favoriteTasks = state.favoriteTasks;

      !task.isDone!
          ? {
              pendingTasks = List.from(pendingTasks)..remove(task),
              competedTasks = List.from(competedTasks)
                ..insert(0, task.copyWith(isDone: true))
            }
          : {
              competedTasks = List.from(competedTasks)..remove(task),
              pendingTasks = List.from(pendingTasks)
                ..insert(0, task.copyWith(isDone: false))
            };
      if (task.isFavorite!) {
        favoriteTasks.where((element) => element.id == task.id).first.isDone =
            !task.isDone!;
      }
      emit(TasksLoaded(
        pendingTasks: pendingTasks,
        competedTasks: competedTasks,
        favoriteTasks: favoriteTasks,
        removedTasks: state.removedTasks,
        selectedPageIndex: state.selectedPageIndex,
      ));
    }
  }

  void _onEditTask(EditTask event, Emitter<TasksState> emit) {
    if (state is TasksLoaded) {
      final state = this.state as TasksLoaded;
      final newTask = event.newTask;
      final oldTask = event.oldTask;

      List<Task> pendingTasks = [];
      List<Task> competedTasks = [];
      List<Task> favoriteTasks = [];
      final int indexOfPending = state.pendingTasks.indexOf(oldTask);
      final int indexOfCompeted = state.competedTasks.indexOf(oldTask);
      final int indexOfFavorite =
          state.favoriteTasks.indexWhere((element) => element.id == oldTask.id);
      if (indexOfPending != -1) {
        pendingTasks = List.from(state.pendingTasks);
        pendingTasks[indexOfPending] = pendingTasks[indexOfPending].copyWith(
            title: newTask.title,
            description: newTask.description,
            date: newTask.date);
      }
      if (indexOfCompeted != -1) {
        competedTasks = List.from(state.competedTasks);
        competedTasks[indexOfCompeted] = competedTasks[indexOfCompeted]
            .copyWith(
                title: newTask.title,
                description: newTask.description,
                date: newTask.date);
      }
      if (indexOfFavorite != -1) {
        favoriteTasks = List.from(state.favoriteTasks);
        favoriteTasks[indexOfFavorite] = favoriteTasks[indexOfFavorite]
            .copyWith(
                title: newTask.title,
                description: newTask.description,
                date: newTask.date);
      }

      emit(TasksLoaded(
        pendingTasks: pendingTasks,
        competedTasks: competedTasks,
        favoriteTasks: favoriteTasks,
        removedTasks: state.removedTasks,
        selectedPageIndex: state.selectedPageIndex,
      ));
    }
  }

  void _onFavorite(FavoriteTask event, Emitter<TasksState> emit) {
    if (state is TasksLoaded) {
      final state = this.state as TasksLoaded;
      List<Task> pendingTasks = state.pendingTasks;
      List<Task> favoriteTasks = List.from(state.favoriteTasks);
      List<Task> competedTasks = state.competedTasks;
      var task = event.task;
      final isFavorite = task.isFavorite!;

      if (!task.isDone!) {
        final indexOfTask = pendingTasks.indexOf(task);
        pendingTasks[indexOfTask].isFavorite = !isFavorite;
      } else {
        final indexOfTask = competedTasks.indexOf(task);
        competedTasks[indexOfTask].isFavorite = !isFavorite;
      }

      if (!isFavorite) {
        favoriteTasks.insert(0, task.copyWith());
      } else {
        favoriteTasks.removeWhere((element) => element.id == task.id);
      }

      emit(TasksLoaded(
        pendingTasks: pendingTasks,
        favoriteTasks: favoriteTasks,
        competedTasks: competedTasks,
        removedTasks: state.removedTasks,
        selectedPageIndex: state.selectedPageIndex,
      ));
    }
  }

  void _onDeleteTask(DeleteTask event, Emitter<TasksState> emit) {
    if (state is TasksLoaded) {
      final state = this.state as TasksLoaded;
      emit(TasksLoaded(
        pendingTasks: state.pendingTasks,
        favoriteTasks: state.favoriteTasks,
        competedTasks: state.competedTasks,
        removedTasks: List.from(state.removedTasks)..remove(event.task),
        selectedPageIndex: state.selectedPageIndex,
      ));
    }
  }

  void _onRemoveTask(RemoveTask event, Emitter<TasksState> emit) {
    if (state is TasksLoaded) {
      final state = this.state as TasksLoaded;
      emit(TasksLoaded(
        pendingTasks: List.from(state.pendingTasks)..remove(event.task),
        favoriteTasks: List.from(state.favoriteTasks)
          ..removeWhere((element) => element.id == event.task.id),
        competedTasks: List.from(state.competedTasks)..remove(event.task),
        removedTasks: List.from(state.removedTasks)
          ..add(event.task.copyWith(isDeleted: true)),
        selectedPageIndex: state.selectedPageIndex,
      ));
    }
  }

  void _onRecycleTask(RecycleTask event, Emitter<TasksState> emit) {
    if (state is TasksLoaded) {
      final state = this.state as TasksLoaded;
      final task = event.task;
      List<Task> pendingTasks = <Task>[];
      List<Task> competedTasks = <Task>[];
      List<Task> favoriteTasks = <Task>[];
      if (task.isDone!) {
        competedTasks = List.from(state.competedTasks)
          ..add(event.task.copyWith(isDeleted: false));
      } else {
        pendingTasks = List.from(state.pendingTasks)
          ..add(event.task.copyWith(isDeleted: false));
      }
      if (task.isFavorite!) {
        favoriteTasks = List.from(state.favoriteTasks)
          ..add(event.task.copyWith(isDeleted: false));
      }
      emit(TasksLoaded(
        pendingTasks: pendingTasks,
        favoriteTasks: favoriteTasks,
        competedTasks: competedTasks,
        removedTasks: List.from(state.removedTasks)..remove(event.task),
        selectedPageIndex: state.selectedPageIndex,
      ));
    }
  }

  @override
  TasksState fromJson(Map<String, dynamic> json) {
    return TasksLoaded.fromMap(json);
  }

  @override
  Map<String, dynamic>? toJson(TasksState state) {
    if (state is TasksLoaded) {
      return state.toMap();
    }
    return null;
  }
}
