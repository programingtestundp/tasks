import 'package:flutter/material.dart';

import '../screens/pending_task_screen.dart';
import '../screens/recycle_bin.dart';
import '../screens/tabs_screen.dart';

class AppRouter {
  Route? onGenerateRoute(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case RecycleBin.id:
        return MaterialPageRoute(builder: (_) => const RecycleBin());

      case PendingTasksScreen.id:
        return MaterialPageRoute(builder: (_) => const PendingTasksScreen());
      case TabsScreen.id:
        return MaterialPageRoute(builder: (_) => TabsScreen());

      default:
        return null;
    }
  }
}
